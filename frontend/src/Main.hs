{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Arrow ((&&&))
import qualified Data.ByteString.Lazy as B
import Data.Map.Strict (Map, fromList)
import Data.Sas.Loader
import Data.Sas.Measurement
import Data.Text (Text, intercalate, pack)
import qualified Data.Vector.Unboxed as V
import Reflex.Dom
import Text.Printf

both :: (a -> b) -> (a, a) -> (b, b)
both f (x, y) = (f x, f y)

main :: IO ()
main = do
  f <- B.readFile "example.xml"
  let dat = loadXml f
      columns = both dataColumnValues . (_measurementIndependent   &&& _measurementDependent) <$> dat
  mainWidget $ myDiv columns

myDiv :: (DomBuilder t m, PostBuild t m) => (Either String (V.Vector Double , V.Vector Double)) -> m ()
myDiv (Right (xs, ys)) = do
  let attrs = constDyn $ fromList
               [ ("width" , "500")
               , ("height" , "500")
               ]
  let cAttrs = constDyn $ fromList
               [
               ("d", pointsToPath xs ys)
               , ("stroke", "green")
               , ("stroke-width", "3")
               , ("fill",  "none" )
               ]

  -- s <- elSvg "svg" attrs (elSvg "circle" cAttrs (return ()))
  -- text $ pointsToPath xs ys
  s <- elSvg "svg" attrs (elSvg "path" cAttrs $ return ())
  return ()

rescale ::  Double -> V.Vector Double -> V.Vector Double
rescale top values =
  let low = V.minimum values
      high = V.maximum values
  in
    V.map (\ x -> top * (x-low)/(high-low)) values

pointsToPath :: V.Vector Double -> V.Vector Double -> Text
pointsToPath xs ys = "m " <> intercalate " L " points
  where points = zipWith (\ x y -> pack (show x) <> " " <> pack (show y)) (V.toList . rescale 500 $ xs') (V.toList . V.map (500 -) . rescale 500 $ ys')
        inner :: V.Vector (Double, Double)
        inner = V.filter (\(x, y) -> not (isNaN x || isNaN y || isInfinite x || isInfinite y)) . V.zipWith (\x y -> (log x, log y)) xs $ ys
        xs' = V.map fst inner
        ys' = V.map snd inner

elSvg :: (DomBuilder t m, PostBuild t m) => Text -> Dynamic t (Map Text Text) -> m a -> m ()
elSvg tag a1 a2 = do
  elDynAttrNS' ns tag a1 a2
  return ()

ns :: Maybe Text
ns = Just "http://www.w3.org/2000/svg"
