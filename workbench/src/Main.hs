module Main where

import qualified Data.ByteString.Lazy as B
import Data.Sas.Loader
import Data.Sas.Measurement
import Data.Sas.Model
import Data.Sas.Fit
import qualified Data.Vector as V
import Numeric.GSL.Minimization

model = Linear_ $ Linear 5 0.01

example :: Fitter Double
example = gslFitter (-4) 50

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  infile <- B.readFile "/home/adam/Code/sasview/test/sasdataloader/test/ISIS_1_1.xml"
  print $ ( flip example model <$> loadXml infile)
