module Data.Sas.Fit.Util (chi, aChi) where

import qualified Data.Array.Accelerate as A
import Data.List (foldl')
import Data.Sas.Measurement
import Data.Sas.Model (scatter)
import qualified Data.Vector.Unboxed as V

chi ::  (Double -> Double) -> Measurement Double -> Double
chi f measurement =
  let x = _measurementIndependent measurement
      y = _measurementDependent measurement
      m = V.map f $ dataColumnValues x
      resids =
        case y of
          Uncertain i d -> V.zipWith (/) (V.zipWith (-) i m) d
          Raw i ->  V.zipWith (/) (V.zipWith (-) i m) i
  in
    V.foldl' (nanAdd) 0 $ V.map (** 2) resids

aChi :: (A.Exp Double -> A.Exp Double) -> (A.Acc (A.Vector Double)) -> (A.Acc (A.Vector Double)) -> A.Acc (A.Scalar Double)
aChi f x y =
  let iy = A.map f y
      resids = A.zipWith (/) (A.zipWith (-) iy y) y
  in
    A.fold (+) 0 resids

isFinite x = not (isInfinite x || isNaN x)

nanAdd a b
  | (isFinite a) && (isFinite b) = a+b
  | (isFinite a) = a
  | (isFinite b) = b
  | otherwise = 0
