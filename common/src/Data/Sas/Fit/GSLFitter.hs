{-# LANGUAGE ScopedTypeVariables, InstanceSigs, FlexibleContexts #-}

module Data.Sas.Fit.GSLFitter (gslFitter) where

import Data.Sas.Measurement (Measurement)
import Data.Sas.Fit.Util (chi)
import Data.Sas.Fit.Types (Fitter(..))
import Data.Sas.Model
import Numeric.GSL.Minimization (minimize, MinimizeMethod(..))

gslFitter :: Double -> Int -> Fitter Double
gslFitter precision steps measurement rmodel =
  fromList rmodel result
  where
    f :: [Double] -> Double
    f x = chi (scatter rmodel) measurement
    result =
      fst $ minimize NMSimplex precision steps (getRange rmodel) f (toList rmodel)
