{-# LANGUAGE ScopedTypeVariables, InstanceSigs #-}

module Data.Sas.Fit.Types (Fitter) where

import Data.Sas.Measurement (Measurement)
import Data.Sas.Model (Model)

type Fitter x = Measurement x -> Model x -> Model x
