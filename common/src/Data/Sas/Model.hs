module Data.Sas.Model (
  Model(..)
  , Sphere(..)
  , Linear(..)
  , scatter
  , toList
  , fromList
  , getRange
  , aScatter
  , toGpu
  ,fromGpu
  ) where

import Data.Sas.Measurement
import Data.Sas.Model.Linear
import Data.Sas.Model.Sphere
import Data.Sas.Model.Types
import Data.Word

data Model x =
  Sphere_ (Sphere x)
  | Linear_ (Linear x)
  deriving (Show, Eq, Ord)

scatter :: (Floating x) => Model x -> x -> x
scatter (Linear_ x) = linearScatter x
scatter (Sphere_ x) = sphereScatter x

toList :: Model x -> [x]
toList (Sphere_ x) = sphereToList x
toList (Linear_ x) = linearToList x

fromList (Sphere_ _) = Sphere_ . sphereFromList
fromList (Linear_ _) = Linear_ . linearFromList

getRange :: (Num x) => Model x -> [x]
getRange (Sphere_ x) = sphereGetRange x
getRange (Linear_ x) = linearGetRange x

toGpu :: Model x -> (Word8, GpuModel x)
toGpu (Sphere_ (Sphere r s)) = (0, (r, s))
toGpu (Linear_ (Linear m b)) = (1, (m, b))

fromGpu :: (Word8, GpuModel x) -> Model x
fromGpu (0, x) = Sphere_ $ sphereFromGpu x
fromGpu (1, (m, b)) = Linear_ $ Linear m b

aScatter ::(Floating a) => (Word8, GpuModel a) -> a -> a
aScatter (0, x) = sphereAScatter x
