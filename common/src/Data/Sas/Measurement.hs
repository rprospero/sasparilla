module Data.Sas.Measurement where

import qualified Data.Vector.Unboxed as V

data DataColumn a =
  Uncertain (V.Vector a) (V.Vector a)
  | Raw (V.Vector a)
  deriving Show

dataColumnValues :: DataColumn a -> V.Vector a
dataColumnValues (Uncertain v _) = v
dataColumnValues (Raw v) = v


data Header = Header
  deriving Show

data Measurement a = Measurement {
  _measurementHeader :: Header
  , _measurementIndependent :: DataColumn a
  , _measurementDependent :: DataColumn a
} deriving (Show)
