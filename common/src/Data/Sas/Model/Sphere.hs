module Data.Sas.Model.Sphere (
  Sphere(..)
  , sphereScatter
  , sphereToList
  , sphereFromList
  , sphereGetRange
  , sphereToGpu
  , sphereFromGpu
  , sphereAScatter
    ) where

import qualified Data.Array.Accelerate as A
import Data.Array.Accelerate (Exp(..), Plain(..))
import Data.Array.Accelerate.Array.Sugar (EltRepr(..), Elt(..))
import Data.Array.Accelerate.Product
import Data.Array.Accelerate.Smart
import Data.Array.Accelerate.Type
import Data.Sas.Model.Types
import Data.Typeable
import GHC.Generics

data  Sphere a = Sphere {
  radius :: a
  , scale :: a
} deriving (Eq, Show, Typeable, Ord, Bounded)

sphereScatter (Sphere r s) = scatter r s

scatter r s q =
  let qr = q * r
  in
    s * ((sin qr - qr * cos qr) / qr ** 3)**2
sphereToList (Sphere r s) = [r, s]
sphereFromList [r, s] = Sphere r s
sphereGetRange _ = [1, 1]

sphereToGpu (Sphere r s) = (r, s)
sphereFromGpu = uncurry Sphere

sphereAScatter (r, s) = scatter r s
