module Data.Sas.Model.Linear (
  Linear(..)
  , linearScatter
  , linearToList
  , linearFromList
  , linearGetRange
  ) where

import Data.Sas.Model.Types

data Linear a = Linear {
  slope :: a
  , intercept :: a
} deriving (Show, Eq, Ord, Bounded)

linearScatter (Linear m b) q = m * q + b
linearToList (Linear m b) = [m, b]
linearFromList [m, b] = Linear m b
linearGetRange _ = [1, 1]
