{-# LANGUAGE OverloadedStrings #-}

module Data.Sas.Loader (loadXml) where

import Control.Exception.Base
import Control.Lens
import qualified Data.ByteString.Lazy as B
import Data.Default.Class
import Data.Either.Combinators (mapLeft)
import Data.Sas.Measurement
import qualified Data.Text.Lazy as T
import Data.Text.Lazy.Encoding
import Data.Text.Read
import qualified Data.Vector.Unboxed as V
import Text.XML
import Text.XML.Lens


loadXml :: B.ByteString -> Either String (Measurement Double)
loadXml txt = extractColumns =<< (mapLeft show . parseText def . decodeUtf8 $ txt)

extractColumns :: Document -> Either String (Measurement Double)
extractColumns doc =
  let
    dataPoint = root . named "SASroot" ./ named "SASentry" ./ named "SASdata" ./ named "Idata"
    x = doc ^.. dataPoint ./ named "Q" . text
    dx = doc ^.. dataPoint ./ named "Qdev" . text
    y = doc ^.. dataPoint ./ named "I" . text
    dy = doc ^.. dataPoint ./ named "Idev" . text
    withError a da
      | length a == length da = Uncertain (V.fromList a) (V.fromList da)
      | otherwise = Raw . V.fromList $ a
    parse = sequence . map (fmap fst) . map double
  in
    do
      q <- parse x
      dq <- parse dx
      iq <- parse y
      diq <- parse dy
      return $ Measurement Header (q `withError` dq) (iq `withError` diq)
