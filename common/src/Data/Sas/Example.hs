module Data.Sas.Example (main) where

import qualified Data.ByteString.Lazy as B
import qualified Data.Array.Accelerate as A
import Data.Array.Accelerate.IO.Data.Vector.Unboxed (fromUnboxed)
import Data.Array.Accelerate.Interpreter (run)
import Data.Sas.Fit
import Data.Sas.Fit.Util (aChi)
import Data.Sas.Measurement
import Data.Sas.Model
import Data.Sas.Loader (loadXml)

main :: Bool -> IO ()
main test = do
  dat <- B.readFile "example.xml"
  let model =
        if test
        then Sphere_ $ Sphere 100.0 10.0
        else Linear_ $ Linear 1.0 2.0
  case loadXml dat of
    Left err -> print err
    Right x -> do
      let result = gslFitter 1e-3 100 x model
      print result

gpu :: IO ()
gpu = do
  fdat <- B.readFile "example.xml"
  case loadXml fdat of
    Right dat -> do
      let model = Sphere_ $ Sphere 100.0 10.0
          x = fromUnboxed $ dataColumnValues $ _measurementDependent dat
          y = fromUnboxed $ dataColumnValues $ _measurementIndependent dat
      print $ run $ aChi (aScatter $ toGpu model) (A.use x) (A.use y)
    Left error -> print error
