# default.nix
(import ./reflex-platform {}).project ({ pkgs, ... }: {
  packages = {
    common = ./common;
    frontend = ./frontend;
  };

  shells = {
    ghc = ["common" "frontend"];
  };

  overrides = self: super: {
    accelerate = pkgs.haskell.lib.dontCheck super.accelerate;
    system-fileio = pkgs.haskell.lib.dontCheck super.system-fileio;
  };
}) 
